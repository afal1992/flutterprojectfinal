## Description

This project is a mobile application, it was build with Flutter Framework

 ## Frameworks
1. [Flutter](https://flutter.dev/)
2. [Firebase](https://firebase.google.com/?hl=es)

## Manual Installation
Optional you can create new keystore
1. keytool -genkey -v -keystore ~/upload-keystore.jks -keyalg RSA -keysize 2048 -validity 10000 -alias upload
2. Replace keystore properties in [project]/android/key.properties 
```bash
$ cd [project]
$ flutter pub clean
$ flutter pub get
$ flutter build apk --split-per-abi
$ Optional generate bundle: flutter build apk --split-per-abi
```
