import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Util {
  static void showGenericError(BuildContext context,callback) {


    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text((AppLocalizations.of(context)?.titleErrProcess ?? "")),
          content: Text((AppLocalizations.of(context)?.messageErrProcess ?? "")),
          actions: [
        TextButton(

        child: Text("Aceptar"),
        onPressed: () {
        if(callback != null) callback();
        Navigator.pop(context);

        },
        )
          ],
        );
      },
    );
  }

  static void showMessage(BuildContext context,callback,String title,String message) {


    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: Text(message),
          actions: [
            TextButton(

              child: Text("Aceptar"),
              onPressed: () {
                if(callback != null) callback();
                Navigator.pop(context);

              },
            )
          ],
        );
      },
    );
  }

  static Future<Customer?> getUser() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      if (prefs == null) {
        return null;
      }

      String? usuarioJson = prefs.getString("user");
      if (usuarioJson == null || usuarioJson.isEmpty) {
        return null;
      }

      return Customer.fromJson(jsonDecode(usuarioJson));
    } catch (e) {
      print(e);
      return null;
    }
  }

}
