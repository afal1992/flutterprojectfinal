import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:io';

import 'package:management_location_mobile/connection/generic_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FetchServer {
  static var URL_BASE = "http://45.63.77.183:3002/";

  static Future<GenericResponse> fetchPost(
      String endPoint, Object dataBody) async {
    try {
      if (dataBody == null) dataBody = new HashMap<String, String>();

      String urlFetch = '$URL_BASE$endPoint';

      HttpClient client = new HttpClient();
      client.badCertificateCallback =
          ((X509Certificate cert, String host, int port) {
        final isValidHost = host == urlFetch.replaceAll("https://", "");
        return true;
      });

      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? session = prefs.getString('accessToken');

      final request = await client.postUrl(Uri.parse(urlFetch));
      request.headers.set("Content-Type", "application/json; charset=UTF-8");
      if (session!=null && session.isNotEmpty) {
        request.headers.set("Authorization", 'Bearer $session');
      }
      request.write(jsonEncode(dataBody));

      final response = await request.close();

      if (response.statusCode >= 200 && response.statusCode <= 299) {
        return GenericResponse(false, await getCompleterRequest(response).future);
      } else if (response.statusCode == 401) {
        return GenericResponse(true, await getCompleterRequest(response).future);
      } else {
        return getErrorResponse();
      }
    } catch (e) {
      print(e);
      return getErrorResponse();

    }
  }

  static Completer getCompleterRequest(HttpClientResponse response){
    var completer = Completer();
    var contents = StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));

    return completer;
  }

  static GenericResponse getErrorResponse(){
    Map<String,dynamic> nestedObjText = {"error_description": "Failed to load jobs from API"};
    return GenericResponse(true, nestedObjText);
  }
}
