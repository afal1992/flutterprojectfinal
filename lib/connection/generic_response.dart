class GenericResponse{

  bool error = false;
  Object? responseRequest;

  GenericResponse(this.error, this.responseRequest);

}