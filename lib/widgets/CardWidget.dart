import 'package:flutter/material.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/restaurant.dart';
import 'package:management_location_mobile/pages/detail_publication.dart';
import 'package:management_location_mobile/pages/save_postulation.dart';

class CardWidget extends StatelessWidget {
  Publication publication;
  bool showPostulation;

  CardWidget({Key? key, required this.publication,required this.showPostulation}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 292,
      margin: EdgeInsets.only(left: 20, right: 20, top: 15, bottom: 20),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
              color: Theme.of(context).focusColor.withOpacity(0.1),
              blurRadius: 15,
              offset: Offset(0, 5)),
        ],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // Image of the card
          Hero(
            tag: publication.id ?? "",
            child: Container(
              width: 292,
              height: 90,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(publication.user!.picture)
                    , fit: BoxFit.cover),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  flex: 4,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        publication.title ?? "",
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                      Text(
                        publication.description ?? "",
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.caption,
                      ),
                      SizedBox(height: 5),
                      Text(
                        publication.priority ?? "",
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ],
                  ),
                ),
                SizedBox(width: 10),
                showPostulation ? Expanded(
                  child: FlatButton(
                    padding: EdgeInsets.all(0),
                    onPressed: () {
                      Navigator.push(context,MaterialPageRoute(builder: (context) =>  SavePostulation(publication: publication,)),);

                    },
                    child: Icon(Icons.directions,
                        color: Theme.of(context).primaryColor),
                    color: Theme.of(context).accentColor,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5)),
                  ),
                ):new Container(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
