import 'package:flutter/material.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/review.dart';
import 'package:management_location_mobile/widgets/PostulationItemWidget.dart';

// ignore: must_be_immutable
class PostulationListWidget extends StatelessWidget {
  List<Publication> publicationList;

  PostulationListWidget({
    Key? key, required this.publicationList
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.all(0),
      itemBuilder: (context, index) {
        return PostulationItemWidget(postulation: publicationList.elementAt(index));
      },
      separatorBuilder: (context, index) {
        return SizedBox(height: 20);
      },
      itemCount: publicationList.length,
      primary: false,
      shrinkWrap: true,
    );
  }
}
