import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/review.dart';
import 'package:management_location_mobile/pages/detail_publication.dart';

// ignore: must_be_immutable
class PostulationItemWidget extends StatelessWidget {
  Publication postulation;

  PostulationItemWidget({Key? key, required this.postulation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context,MaterialPageRoute(builder: (context) =>  DetailPublication(publication: postulation,)),);

      },
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 10,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 65,
                width: 65,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                  image: DecorationImage(
                      image: NetworkImage(postulation.user!.picture)
                      , fit: BoxFit.cover),
                ),
              ),
              SizedBox(width: 15),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            postulation.title ?? "",
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.subtitle1!.merge(
                                TextStyle(color: Theme.of(context).hintColor)),
                          ),
                        ),
                        SizedBox(
                          width: 35,
                          height: 35,
                          child: FlatButton(
                            padding: EdgeInsets.all(0),
                            onPressed: () {},
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                getStatus(postulation.statusPostulation,context)
                              ],
                            ),
                            color:
                                Theme.of(context).accentColor.withOpacity(0.9),
                            shape: StadiumBorder(),
                          ),
                        ),
                      ],
                    ),
                    Text(
                      postulation.createDate != null ?
                      new DateFormat('yyyy-MM-dd hh:mm').format(postulation.createDate!) : "",
                      style: Theme.of(context).textTheme.caption,
                    )
                  ],
                ),
              )
            ],
          ),
          Text(
            postulation.description ?? "",
            style: Theme.of(context).textTheme.bodyText1,
            overflow: TextOverflow.ellipsis,
            softWrap: false,
            maxLines: 3,
          )
        ],
      ),
    );
  }

  Widget getStatus(String? status,BuildContext context){
    if(status == null || status.isEmpty) return Icon(
      Icons.watch_later,
      color: Theme.of(context).primaryColor,
      size: 16,
    );

    if(status == "pending"){
      return Icon(
        Icons.watch_later,
        color: Theme.of(context).primaryColor,
        size: 16,
      );
    }
    if(status == "refused"){
      return Icon(
        Icons.close,
        color: Colors.red,
        size: 16,
      );
    }
    if(status == "accept"){
      return Icon(
        Icons.close,
        color: Colors.green,
        size: 16,
      );
    }
    return Icon(
      Icons.watch_later,
      color: Theme.of(context).primaryColor,
      size: 16,
    );
  }
}
