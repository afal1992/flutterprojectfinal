import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final String texto;
  final ImageProvider? imagem;
  final Color? cor;
  final ElevatedButton? icon;
  final Function callBack;

  const LoginButton({
    Key? key,
    required this.texto,
    this.imagem,
    this.cor,
    this.icon,
    required this.callBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {callBack();},
      style: ButtonStyle(
        elevation: MaterialStateProperty.all(1),
        overlayColor: MaterialStateProperty.all(Colors.black12),
        shadowColor: MaterialStateProperty.all(Colors.pink.shade50),
        shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50))),
        backgroundColor: MaterialStateProperty.all(Colors.white),
        fixedSize: MaterialStateProperty.all(const Size(412, 45)),
      ),
      child: Row(
        children: [
          ImageIcon(
            imagem,
            size: 20,
            color: cor,
          ),
          const Spacer(),
          Text(
            texto,
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.black87,
                fontSize: 15,
                fontWeight: FontWeight.w500),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
