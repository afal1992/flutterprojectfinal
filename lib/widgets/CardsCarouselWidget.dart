import 'package:flutter/material.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/restaurant.dart';
import 'package:management_location_mobile/pages/detail_publication.dart';
import 'package:management_location_mobile/widgets/CardWidget.dart';

class CardsCarouselWidget extends StatefulWidget {
  final List<Publication> publicationList;
  bool showPostulation;

  CardsCarouselWidget({Key? key,required this.publicationList,required this.showPostulation}) : super(key: key);

  @override
  _CardsCarouselWidgetState createState() => _CardsCarouselWidgetState();
}

class _CardsCarouselWidgetState extends State<CardsCarouselWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.publicationList.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(context,MaterialPageRoute(builder: (context) =>  DetailPublication(publication: widget.publicationList[index],)),);

            },
            child: CardWidget(publication: widget.publicationList[index],showPostulation: widget.showPostulation,),
          );
        },
      ),
    );
  }
}
