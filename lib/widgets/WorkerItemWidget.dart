import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/model/review.dart';
import 'package:management_location_mobile/pages/detail_publication.dart';
import 'package:management_location_mobile/pages/profile_detail_worker.dart';

// ignore: must_be_immutable
class WorkerItemWidget extends StatelessWidget {
  Customer user;
  bool showCat;

  WorkerItemWidget({Key? key, required this.user,required this.showCat}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context,MaterialPageRoute(builder: (context) =>  ProfileDetailWorkerPage(user: user,)),);

      },
      child: Wrap(
        direction: Axis.horizontal,
        runSpacing: 10,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 65,
                width: 65,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(100)),
                  image: DecorationImage(
                      image: NetworkImage(user!.picture)
                      , fit: BoxFit.cover),
                ),
              ),
              SizedBox(width: 15),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            user.name,
                            overflow: TextOverflow.fade,
                            softWrap: false,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.subtitle1!.merge(
                                TextStyle(color: Theme.of(context).hintColor)),
                          ),
                        ),

                      ],
                    ),

                    Text(
                      user.email,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      maxLines: 2,
                      style: Theme.of(context).textTheme.subtitle1!.merge(
                          TextStyle(color: Theme.of(context).focusColor,fontSize: 13)),
                    ),

                    user.descPostulation != null ? Text(
                      user.descPostulation!,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      maxLines: 2,
                      style: Theme.of(context).textTheme.caption,

                    ): new Container()
                  ],
                ),
              ),
            ],
          ),
          showCat ? Row(children: getCategoryList(),) : new Container(),
          /*!showCat  ? Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[

                GestureDetector(onTap: (){}, child:
                Icon(Icons.check,color: Colors.green,),),
                GestureDetector(onTap: (){}, child:
                Icon(Icons.close,color: Colors.red,),),

              ]):new Container()*/
        ],
      ),
    );


  }

  List<Widget> getCategoryList(){

    if(user.categoryExperience == null) return [];
    List<Widget> widgetList = [];

    for(var item in user.categoryExperience!){
        widgetList.add(Chip(
        avatar: CircleAvatar(
          backgroundColor: Colors.grey.shade800,
          child: Text(item.name.substring(0,1)),
        ),
        label: Text(item.name),
      ));
    }
    var widgetFilter = widgetList;
    if(widgetList.length >= 3){
      widgetFilter = widgetList.sublist(0,2);
    }
    return widgetFilter;
  }




  Widget getStatus(String? status,BuildContext context){
    if(status == null || status.isEmpty) return Icon(
      Icons.watch_later,
      color: Theme.of(context).primaryColor,
      size: 16,
    );

    if(status == "pending"){
      return Icon(
        Icons.watch_later,
        color: Theme.of(context).primaryColor,
        size: 16,
      );
    }
    if(status == "refused"){
      return Icon(
        Icons.close,
        color: Colors.red,
        size: 16,
      );
    }
    if(status == "accept"){
      return Icon(
        Icons.close,
        color: Colors.green,
        size: 16,
      );
    }
    return Icon(
      Icons.watch_later,
      color: Theme.of(context).primaryColor,
      size: 16,
    );
  }
}
