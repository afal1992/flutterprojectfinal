import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
enum AniProps { opacity, translateY }
class FadeAnimation extends StatelessWidget {
  final double delay;
  final Widget child;
  final _tween = MultiTween<AniProps>()
      ..add(AniProps.opacity, Tween(begin: 0.0,end: 1.0), const Duration(milliseconds: 500))
      ..add(AniProps.translateY, Tween(begin: -30.0,end: 0.0), const Duration(milliseconds: 500), Curves.easeOut);
  FadeAnimation(this.delay, this.child);

  @override
  Widget build(BuildContext context) {

    return PlayAnimation<MultiTweenValues<AniProps>>(
      delay: Duration(milliseconds: (500 * delay).round()),
      duration: _tween.duration,
      tween: _tween,
      child: child,
        builder: (context, child, value) => Opacity(
          opacity: value.get(AniProps.opacity),
          child: Transform.translate(
            offset: Offset(value.get(AniProps.translateY), 0),
            child: child,
          ),
        ),
    );
  }
}