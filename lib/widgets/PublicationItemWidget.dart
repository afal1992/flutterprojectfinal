import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/order.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:management_location_mobile/pages/detail_publication.dart';

class PublicationItemWidget extends StatelessWidget {
  final String? heroTag;
  final Publication publication;

  const PublicationItemWidget({Key? key, required this.publication, this.heroTag}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).accentColor,
      focusColor: Theme.of(context).accentColor,
      highlightColor: Theme.of(context).primaryColor,
      onTap: () {
        Navigator.push(context,MaterialPageRoute(builder: (context) =>  DetailPublication(publication: publication,)),);
      },
      child: Container(
    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
    decoration: BoxDecoration(

    color: Theme.of(context).accentColor,
    boxShadow: [
    BoxShadow(
    color: Theme.of(context).focusColor.withOpacity(0.1),
    blurRadius: 5,
    offset: Offset(0, 2)),
    ],
    ),
    child:Column(children:[ Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            publication.user != null && publication.user!.picture != null ?Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                image: DecorationImage(
                    image: NetworkImage(publication.user!.picture)
                    , fit: BoxFit.cover),
              ),
            ): new Container(),
            SizedBox(width: 15),
            Flexible(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          publication.title ?? "",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.headline6?.merge(TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Text(
                          publication.description ?? "",
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          publication.createDate != null ?new DateFormat("dd/MM/yyyy").format(publication.createDate!) : "",
                          style: Theme.of(context).textTheme.caption?.merge(TextStyle(color: Theme.of(context).focusColor)),
                        ),


                      ],
                    ),
                  ),
                  SizedBox(width: 8),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[

                      Text(
                        publication.priority ?? "",
                        style: Theme.of(context).textTheme.caption,
                      ),
                    ],
                  ),

                ],
              ),
            ),

          ],

      ),SizedBox(height: 2,), Row(children: getCategoryList(),),])),
    );
  }
  List<Widget> getCategoryList(){

    if(publication.categoryExperience == null) return [];
    List<Widget> widgetList = [];

    for(var item in publication.categoryExperience!){
      widgetList.add(Chip(
        avatar: CircleAvatar(
          backgroundColor: Colors.grey.shade800,
          child: Text(item.name.substring(0,1)),
        ),
        label: Text(item.name),
      ));
    }
    var widgetFilter = widgetList;
    if(widgetList.length >= 3){
      widgetFilter = widgetList.sublist(0,2);
    }
    return widgetFilter;
  }
}
