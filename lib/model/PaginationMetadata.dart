  class PaginationMetadata{
  late int totalItems;
  late int itemCount;
  late int itemsPerPage;
  late int totalPages;
  late int currentPage;

  PaginationMetadata({required this.totalItems, required this.itemCount, required this.itemsPerPage,
    required this.totalPages, required this.currentPage});

  factory PaginationMetadata.fromJson( json) {
    return PaginationMetadata(

      totalItems:json['totalItems']!=null?json['totalItems'] : 0,
      itemCount:json['itemCount']!=null?json['itemCount'] : 0,
      itemsPerPage:json['totalItems']!=null?json['itemsPerPage'] : 0,
      totalPages:json['totalItems']!=null?json['totalPages'] : 0,
      currentPage:json['currentPage']!=null?json['currentPage'] : 0,


    );

  }

  

}