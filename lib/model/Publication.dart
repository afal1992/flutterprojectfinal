import 'dart:ffi';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:intl/intl.dart';
import 'package:management_location_mobile/model/CategoryExperience.dart';
import 'package:management_location_mobile/model/customer.dart';

class Publication{
  late String? id;
  late String? title;
  late String? description;
  late DateTime? createDate;
  late DateTime? endDate;
  late String? priority;
  late String? state;
  late Customer? user;
  late double? latitude;
  late double? longitude;
  late String? statusPostulation;
  late List<CategoryExperience>? categoryExperience = [];
  late String? validateTime = "6";


  Publication({this.id,  this.title,  this.description, this.createDate,
   this.endDate,  this.priority,  this.state,  this.user, this.latitude,  this.longitude,
  this.statusPostulation,this.categoryExperience});

  factory Publication.fromJson( json) {
    List<dynamic> categoryListJson = json['categories']!=null?json['categories']:[];
    List<CategoryExperience> categories = (categoryListJson)
        .map((i) => CategoryExperience.fromJson(i))
        .toList();
    return Publication(

      id:json['id'],
      title:json['title'],
      description:json['description']!=null?json['description'] : "",
      priority:json['priority']!=null?json['priority'] : "",
      state:json['state']!=null?json['state'] : "",
      createDate:DateTime.parse(json['createDate']),
      endDate:json['endDate']!=null?DateTime.parse(json['endDate']):null,
      user: json['customer'] !=null ? Customer.fromJson(json['customer']) : null,
      latitude:json['latitude']!=null && json['latitude'].toString().isNotEmpty ?double.parse(json['latitude'].toString()) : 0,
      longitude:json['longitude']!=null && json['longitude'].toString().isNotEmpty ?double.parse(json['longitude'].toString()) : 0,
      statusPostulation:json['statusPostulation']!=null?json['statusPostulation'] : "",
        categoryExperience:categories



    );



  }

  factory Publication.toPostulation( json) {

    var customerId = json['customer_id'] != null ? json['customer_id'] : "";
    var customerPicture = json['customer_picture'] != null ? json['customer_picture'] : "";
    var customerName = json['customer_name'] != null ? json['customer_name'] : "";
    var user = Customer.simple(customerId, customerName, customerPicture);
    return Publication(

      id: json['pub_id'],
      title: json['pub_title'],
      description: json['pub_description'] != null
          ? json['pub_description']
          : "",
      priority: json['pub_priority'] != null ? json['pub_priority'] : "",
      state: json['pub_state'] != null ? json['pub_state'] : "",
      createDate: DateTime.parse(json['pub_createDate']),
      endDate: json['pub_endDate'] != null
          ? DateTime.parse(json['pub_endDate'])
          : null,
      user: user,
      latitude: json['pub_latitude'] != null && json['pub_latitude']
          .toString()
          .isNotEmpty ? double.parse(json['pub_latitude'].toString()) : 0,
      longitude: json['pub_longitude'] != null && json['pub_longitude']
          .toString()
          .isNotEmpty ? double.parse(json['pub_longitude'].toString()) : 0,
      statusPostulation: json['post_state'] != null ? json['post_state'] : "",


    );
  }
  DateFormat dateFormat = new DateFormat("MM/dd/yyyy HH:mm");

  Map toJson() => {
    'title': title,
    'description': description,
    'priority': priority,
    'state': state,
    'createDateSring': dateFormat.format(createDate!),
    'endDateSring': dateFormat.format(endDate!),
    'latitude': latitude,
    'longitude': longitude,
    'categories': categoryExperience

  };
}