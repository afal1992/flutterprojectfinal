import 'package:management_location_mobile/model/CategoryExperience.dart';

class Customer{
  late String id;
  late String name;
  late String email;
  late String environment;
  late String picture;
  late String? phone;
  late String? direction;
  late String? facebook;
  late String? linkedin;
  late String? instagram;
  late String? descPostulation;
  late List<CategoryExperience>? categoryExperience;


  Customer.simple(this.id, this.name, this.picture);
  Customer({required this.id, required this.name, required this.email,required this.environment,
    required this.picture, this.phone, this.direction, this.facebook,
     this.linkedin, this.instagram, this.categoryExperience,this.descPostulation});

  factory Customer.fromJson( json,{descPost = ""}) {

    List<dynamic> categoryListJson = json['categories']!=null?json['categories']:[];
    List<CategoryExperience> categories = (categoryListJson)
        .map((i) => CategoryExperience.fromJson(i))
        .toList();
    return Customer(

      id:json['id'],
      name:json['name'],
      email:json['email']!=null?json['email'] : "",
      environment:json['environment']!=null?json['environment'] : "",
      picture:json['picture']!=null?json['picture'] : "",
        phone:json['phone']!=null?json['phone'] : "",
    direction:json['direction']!=null?json['direction'] : "",
        facebook:json['facebook']!=null?json['facebook'] : "",
    linkedin:json['linkedin']!=null?json['linkedin'] : "",
    instagram:json['instagram']!=null?json['instagram'] : "",
        categoryExperience:categories,
      descPostulation:descPost,

    );

  }
  Map toJson() => {
    'id': id,
    'name': name,
    'email': email,
    'environment': environment,
    'picture': picture

  };


}