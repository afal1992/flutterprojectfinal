import 'package:flutter/material.dart';
import 'package:management_location_mobile/pages/enviroment_select.dart';
import 'package:management_location_mobile/pages/login_page.dart';
import 'package:management_location_mobile/pages/portfolio_publication.dart';
import 'package:management_location_mobile/pages/tab_bar_home_page.dart';
import 'package:management_location_mobile/pages/tab_bar_home_page_customer.dart';

class RouteGenerator{
  
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/home':
        return MaterialPageRoute(builder: (_) => TabBarHomePage());
      case '/homeClient':
        return MaterialPageRoute(builder: (_) => TabBarHomePageCustomer());
      case '/environment':
        return MaterialPageRoute(builder: (_)=>EnvironmentSelect());
      case '/publication_portfolio':
        return MaterialPageRoute(builder: (_) => PortfolioPublication());

      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();

    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('Error'),
        ),
        body: const Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}