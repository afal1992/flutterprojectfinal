import 'package:flutter/material.dart';
import 'package:management_location_mobile/provider/SelectMenuProvider.dart';
import 'package:management_location_mobile/route/RouteGenerator.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Firebase.initializeApp().whenComplete(() {
      print("Firebase Initialize");
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
        ChangeNotifierProvider(create: (_) => SelectMenuProvider()),
    ],
    child:MaterialApp(
      title: 'Company Name',
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en', ''),
        Locale('es', ''),
      ],

      theme: ThemeData(
        fontFamily: 'Poppins',
        primaryColor: config.Colors().accentColor(1),
        brightness: Brightness.dark,
        accentColor: config.Colors().mainColor(1),
        focusColor: config.Colors().thirdColor(1),
        hintColor: config.Colors().secondColor(1),

          primaryColorLight: config.Colors().mainDarkColor(1),
        textTheme: TextTheme(
          headline1: TextStyle(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
              color: config.Colors().secondColor(1)),
          headline2: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.w600,
              color: config.Colors().secondColor(1)),
          headline3: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.w700,
              color: config.Colors().mainColor(1)),
          headline4: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.w300,
              color: config.Colors().secondColor(1)),
          headline6: TextStyle(
              fontSize: 14.0,
              fontWeight: FontWeight.w300,
              color: config.Colors().secondColor(1)),
          subtitle2: TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.w500,
              color: config.Colors().secondColor(1)),
          subtitle1: TextStyle(
              fontSize: 16.0,
              fontWeight: FontWeight.w600,
              color: config.Colors().mainColor(1)),
          bodyText1:
              TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
          bodyText2:
              TextStyle(fontSize: 14.0, color: config.Colors().secondColor(1)),
          caption:
              TextStyle(fontSize: 12.0, color: config.Colors().accentColor(1)),
        ),
      ),
      initialRoute: '/environment',
      onGenerateRoute: RouteGenerator.generateRoute,

    ));
  }
}
