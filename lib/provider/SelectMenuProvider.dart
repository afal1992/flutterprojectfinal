import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class SelectMenuProvider with ChangeNotifier, DiagnosticableTreeMixin {
  int _currentTab = 0;

  int get currentTab => _currentTab;

  void setCurrentTab(int currentTab) {
    _currentTab = currentTab;
    notifyListeners();
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(IntProperty('currentTab', currentTab));
  }
}