import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/PaginationMetadata.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/pages/portfolio_publication.dart';
import 'package:management_location_mobile/provider/SelectMenuProvider.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/CardsCarouselWidget.dart';
import 'package:management_location_mobile/widgets/CaregoriesCarouselWidget.dart';
import 'package:management_location_mobile/widgets/FadeAnimation.dart';
import 'package:management_location_mobile/widgets/FoodsCarouselWidget.dart';
import 'package:management_location_mobile/widgets/PostulationListWidget.dart';
import 'package:management_location_mobile/widgets/SearchBarWidget.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget{

  @override
  StateHomePage createState() => StateHomePage();

}

class StateHomePage extends State<HomePage>{

    @override
    void initState() {
      this.getPostulationArray();
      this.getLastPublication();
      super.initState();
    }

  List<Publication>? postulationArray;
  List<Publication>? lastPublicationArray;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.only(top: 5, left: 20, right: 20),
            child: ListTile(
              dense: true,
              contentPadding: EdgeInsets.symmetric(vertical: 0),
              leading: Icon(
                Icons.stars,
                color: Theme.of(context).hintColor,
              ),
              title: Text(
                AppLocalizations.of(context)!.last_publications,
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
          ),
          lastPublicationArray != null ? CardsCarouselWidget(publicationList: lastPublicationArray!,showPostulation: true,) : new Container(),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: ListTile(
              dense: true,
              contentPadding: EdgeInsets.symmetric(vertical: 5),
              leading: Icon(
                Icons.recent_actors,
                color: Theme.of(context).hintColor,
              ),
              title: Text(
                AppLocalizations.of(context)?.last_postulations ?? "",
                style: Theme.of(context).textTheme.headline6,
              ),
            ),
          ),
          postulationArray == null || postulationArray!.isEmpty ?
          Container(
              padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
              child: Column(children: [
                Image(image: AssetImage('assets/images/emptypostulation.png')),
                Text(AppLocalizations.of(context)!.empty_postulation_msg,style: TextStyle(fontSize:18,color: Theme.of(context).primaryColor))
                ,SizedBox(height: 20,),
                FadeAnimation(0.5, Container(
                    height: 50,
                    width: 250,

                    // margin: EdgeInsets.symmetric(horizontal: 50),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Color(0xFF464646)
                    ),
                    child: GestureDetector(
                      onTap: () {
                        context.read<SelectMenuProvider>().setCurrentTab(2);
                      },
                      child: Center(
                        child: Text(AppLocalizations.of(context)!.start_postulation, style: TextStyle(fontSize: 18,color: Color(0xFFcccccf), fontWeight: FontWeight.bold),),
                      ),
                    )
                )),

              ],)


          ): Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: PostulationListWidget(publicationList: postulationArray!,),
          ),


        ],
      ),
    );
  }

  void getPostulationArray() async {
    try{

      var responsePostulationArray = await FetchServer.fetchPost("worker/getPostulationCustomerArray", {});
      if(responsePostulationArray.error || responsePostulationArray.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responsePostulationArray.responseRequest.toString());
      final List<dynamic> data = responseRequest['data'];

      List<Publication> postulationArray = (data)
          .map((i) => Publication.toPostulation(i))
          .toList();

      setState(() {
        this.postulationArray = postulationArray;
      });


    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});
    }
  }

  void getLastPublication() async {
    try {
      var dataPagination = {"page": 1, "limit": 30};
      var responsePagination = await FetchServer.fetchPost("worker/getPublicationArray",dataPagination);
      if(responsePagination.error || responsePagination.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responsePagination.responseRequest.toString());
      final Map<String,dynamic> data = responseRequest['data'];
      final List<dynamic> publicationArrayResp = data['items'];
      final Map<String,dynamic> paginationResp = data['meta'];

      List<Publication> publicationArray = (publicationArrayResp)
          .map((i) => Publication.fromJson(i))
          .toList();


      setState(() {
        lastPublicationArray = publicationArray;
      });






    } catch (e) {
      print(e);
      Util.showGenericError(context, () => {});
    }
  }
}