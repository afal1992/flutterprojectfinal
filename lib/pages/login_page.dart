import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/connection/generic_response.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/pages/enviroment_select.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/FadeAnimation.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: <String>[
    'openid',
    'profile',
    'email',
  ],
);

class LoginPage extends StatefulWidget {
  final String environment;
  const LoginPage({Key? key,required this.environment}) : super(key: key);

  @override
  StateLoginpage createState() => StateLoginpage();
}

class StateLoginpage extends State<LoginPage> {
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtPass = TextEditingController();
  bool validEmail = false;
  bool validCelular = false;
  bool validPass = false;
  bool obscurePass = true;
  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(

      child: Scaffold(
        body:Container(

            width: double.infinity,
            decoration: const BoxDecoration(
              color: Color(0xFF212121),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,

              children: [



                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[




                    SizedBox(height: 150),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: FadeAnimation(0.2,Text(
                        widget.environment == EnvironmentSelect.ENV_WORKER ? 'Unete a la mas grande comunidad \nde profesionales y \nofrece tus servicios' :
                        'Unete a la mas grande comunidad \nde profesionales y encuentra \nlas persona mas capacitadas \n para ayudarte',
                        style: TextStyle(
                            color: Color(0xFFcccccf),
                            fontWeight: FontWeight.bold,
                            fontSize: 20

                        ),
                      )),
                    ),

                    SizedBox(height: 20),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: FadeAnimation(0.5, Text(
                        widget.environment == EnvironmentSelect.ENV_WORKER?
                        'Crea tu perfil profesional y \npermite que miles de usuarios puedan llegar a ti':
                        'Crea tu perfil  y \nencuentra que miles de \nprofesionales que pueden ayudarte',
                        style: TextStyle(
                            color: Color(0xFF777779),
                            fontWeight: FontWeight.bold,
                            fontSize: 15

                        ),
                      )),
                    ),



                    SizedBox(height: 120),




                    SizedBox(height: 30,),




                    GestureDetector(
                      onTap: () {
                        signInWithGoogle();
                      },
                      child:Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: FadeAnimation(1, Container(
                            padding: const EdgeInsets.symmetric(vertical: 2),
                            height: 50,
                            width: 250  ,

                            // margin: EdgeInsets.symmetric(horizontal: 50),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              gradient: const LinearGradient(
                                  colors: [
                                    Color(0xFF397AF3),
                                    Color(0xFF397AF3),
                                  ]
                              ),
                            ),
                            child:  Center(
                              child: Row(children:[
                                const Image(image: AssetImage('assets/images/google-icon.png')),
                                Expanded(child: Center(child:Text(AppLocalizations.of(context)!.signin_google, style: const TextStyle(fontSize: 18,color:Colors.white,
                                    fontWeight: FontWeight.bold),)))]),
                            ),
                          )
                          )),
                    ),


                    SizedBox(height: 30,),



                  ],
                ),
              ],

            )

        ),
      ),
    );
  }

  Future<void> signInWithGoogle() async {
    await Firebase.initializeApp();

    final FirebaseAuth _auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();

    if (googleSignInAccount == null) {
      Util.showGenericError(context, null);
      return;
    }
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final UserCredential authResult =
        await _auth.signInWithCredential(credential);
    final User? user = authResult.user;

    String? accessToken = googleSignInAuthentication.accessToken;
    if (user == null ||  accessToken == null || accessToken.isEmpty) {
      Util.showGenericError(context, null);
      return;
    }

    sendRequestLogin(accessToken);
  }

  void sendRequestLogin(String accessToken) async{

    try{

      var dataRequest = {"accessTokenOauth": accessToken, "environment": widget.environment};
      var resposeLogin = await FetchServer.fetchPost("oauth/signIn", dataRequest);
      if(resposeLogin.error || resposeLogin.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      processSuccessLogin(resposeLogin);


    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});
    }


  }

  void processSuccessLogin(GenericResponse responseLogin) async{
    try{

        final Map<String, dynamic> responseRequest = jsonDecode(responseLogin.responseRequest.toString());
        final Map<String,dynamic> data = responseRequest['data'];
      final String user = data['user'].toString();
      final String token = data['accessToken'].toString();

        Customer userPojo = Customer.fromJson(data['user']);

        SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString("user", jsonEncode(userPojo));
      prefs.setString("accessToken", token);
      var route = "";
      if(widget.environment == EnvironmentSelect.ENV_WORKER) {
        route = "/home";
      } else if(widget.environment == EnvironmentSelect.ENV_CLIENT){
        route = "/homeClient";
      }else {return;}

        Navigator.pushNamedAndRemoveUntil(context, route,ModalRoute.withName('/'));

    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});

    }

  }
}



const kNormalText = TextStyle(
  fontSize: 12,
  color: Colors.white,
);

const kUnderlinedText = TextStyle(
  fontSize: 12,
  color: Colors.white,
  decoration: TextDecoration.underline,
  fontWeight: FontWeight.w500,
);
