import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:management_location_mobile/util/util.dart';

class SavePostulation extends StatefulWidget {
  final Publication publication;

  const SavePostulation({Key? key, required this.publication})
      : super(key: key);

  @override
  StateSavePostulation createState() => StateSavePostulation();
}

class StateSavePostulation extends State<SavePostulation> {
  final descKey = new GlobalKey();
  var txtDescripcion = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              color: Theme.of(context)
            .focusColor
            .withOpacity(1),
              icon: Icon(Icons.check),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  savePostulation();
                }
              },
            ),
          ],
          backgroundColor: Theme.of(context).accentColor,
          elevation: 0,
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.postulation,
            style: Theme.of(context)
                .textTheme
                .subtitle1!
                .merge(TextStyle(letterSpacing: 1.3, color: Colors.white)),
          ),
        ),
        body: LoadingOverlay(
          child: SingleChildScrollView(child: Padding(padding:  const EdgeInsets.only(left: 10, right: 10),child:

    Form(
    key: _formKey,
    child:Column(children: [

            SizedBox(height: 10,),
            TextFormField(
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return AppLocalizations.of(context)!.requiredField;
                }
                return null;
              },
              style: TextStyle(color: config.Colors().secondColor(1)),
              key:descKey,
              keyboardType: TextInputType.text,
              controller: txtDescripcion,
              maxLength: 500,
              maxLines: 7,
              decoration: InputDecoration(
                hintText: AppLocalizations.of(context)!.newPostulationDescribe,
                labelStyle:
                TextStyle(color: Theme.of(context).primaryColor),
                contentPadding: EdgeInsets.all(12),
                hintStyle: TextStyle(
                    color: Theme.of(context)
                        .primaryColor
                        .withOpacity(0.4)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .focusColor
                            .withOpacity(0.2))),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .focusColor
                            .withOpacity(0.5))),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Theme.of(context)
                            .focusColor
                            .withOpacity(0.2))),
              ),
            )

          ],))),),
            isLoading: isLoading,
            opacity: 0.8,
            color: Theme.of(context).accentColor,
            progressIndicator: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 10,
                  ),
                  new Text(
                    "",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ],
              ),
            )));
  }


  void savePostulation () async {

    try{

      var publication = {"id": this.widget.publication.id};
      var dataRequest = {"description":this.txtDescripcion.text,
      "publication":publication};

      var responseRequest = await FetchServer.fetchPost("worker/savePostulation", dataRequest);
      if(responseRequest.error || responseRequest.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }

      final Map<String, dynamic> responseData = jsonDecode(responseRequest.responseRequest.toString());
      var error = responseData['error'] ;
      String title = AppLocalizations.of(context)!.postulation;
      if(error == null || error.toString().isEmpty || error.toString().toLowerCase() != "false"){
        Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.savePublicationError);
        return;
      }
      Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.savePublicationSuccess);

    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});
    }

  }
}
