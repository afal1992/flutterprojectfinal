import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/CategoryExperience.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:management_location_mobile/pages/categories_select_page.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileCategoryPage extends StatefulWidget{
  final Customer user;
  const ProfileCategoryPage({Key? key,required this.user}) : super(key: key);

  @override
  StateProfileCategoryPage createState() => StateProfileCategoryPage();

}
class StateProfileCategoryPage extends State<ProfileCategoryPage>{
  @override
  Widget build(BuildContext context) {

    return Scaffold(body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: getCategoryList())),floatingActionButton: Container(
    padding: EdgeInsets.only(top: 100.0),
    child: FloatingActionButton(
    onPressed: () async {
      var response = await (Navigator.push(context,MaterialPageRoute(builder: (context) =>  CategoriesSelectPage()),));
      if(response == null) return;

      List<CategoryExperience> categoryArraySelect = response as List<CategoryExperience>;
      if(categoryArraySelect!=null){
        print(categoryArraySelect);
        var filter = widget.user.categoryExperience!.map((e) => e.id);
        print(filter);
        if( widget.user.categoryExperience != null ) {
          categoryArraySelect.retainWhere((element) =>
              !filter.contains(
                  element.id));
        }

        saveCategories(categoryArraySelect);

      }

    },
    child: new IconTheme(
    data: new IconThemeData(color: Colors.white),
    child: new Icon(Icons.post_add),
    ),
    backgroundColor: Theme.of(context).focusColor,
    ),
    ));
  }

  List<Widget> getCategoryList(){

    if(widget.user.categoryExperience == null) return [];
    List<Widget> widgetList = [];

    for(var item in widget.user.categoryExperience!){
      widgetList.add(Chip(
        avatar: CircleAvatar(
          backgroundColor: Colors.grey.shade800,
          child: Text(item.name.substring(0,1)),
        ),
        label: Text(item.name),
      ));
    }
    return widgetList;
  }

  void saveCategories(List<CategoryExperience> categoryArray) async {

    try{


      var responseSave = await FetchServer.fetchPost("worker/addCategoryUser", categoryArray);
      print(responseSave);
      if(responseSave.error || responseSave.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }

      final Map<String, dynamic> responseData = jsonDecode(responseSave.responseRequest.toString());
      var error = responseData['error'] ;
      String title = AppLocalizations.of(context)!.category;
      if(error == null || error.toString().isEmpty || error.toString().toLowerCase() != "false"){
        Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.addCategoryError);
        return;
      }
      Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.addCategorySuccess);

      setState(() {
        if(widget.user.categoryExperience == null) widget.user.categoryExperience = [];
        widget.user.categoryExperience!.addAll(categoryArray);
      });

    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});
    }

  }
  
}