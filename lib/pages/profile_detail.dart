import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:management_location_mobile/widgets/FadeAnimation.dart';

class ProfileDetailPage extends StatefulWidget{
  final Customer user;
  const ProfileDetailPage({Key? key,required this.user}) : super(key: key);

  @override
  StateProfileDetailPage createState() => StateProfileDetailPage();

}
class StateProfileDetailPage extends State<ProfileDetailPage>{
  @override
  Widget build(BuildContext context) {

    return Scaffold(body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[

      Center(child:
      Container(
        height: 160,
        width: 160,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(80)),
          image: DecorationImage(
              image: NetworkImage(widget.user!.picture)
              , fit: BoxFit.cover),
        ),
      )),
      SizedBox(height: 20,),

      TextFormField(
        enabled: false,
        style: TextStyle(color: config.Colors().secondColor(1)),

        keyboardType: TextInputType.text,
        initialValue: widget.user.email,
        maxLines: 1,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.email),
            labelText: "Email",
          labelStyle:
          TextStyle(color: Theme.of(context).focusColor),
          contentPadding: EdgeInsets.all(12),
          hintStyle: TextStyle(
              color: Theme.of(context)
                  .focusColor
                  .withOpacity(0.7)),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.5))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
        ),
      ),
      SizedBox(height: 20,),

      TextFormField(
        enabled: false,
        keyboardType: TextInputType.text,
        initialValue: widget.user.name,
        maxLines: 1,
        style: TextStyle(color: config.Colors().secondColor(1)),

        decoration: InputDecoration(
          labelText: "Nombre",
          prefixIcon: Icon(Icons.people),

          labelStyle:
          TextStyle(color: Theme.of(context).focusColor),
          contentPadding: EdgeInsets.all(12),
          hintStyle: TextStyle(
              color: Theme.of(context)
                  .focusColor
                  .withOpacity(0.7)),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.5))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
        ),
      ),

     SizedBox(height: 20,),

     Center(child: Container(
            height: 50,
            width: 250,

            // margin: EdgeInsets.symmetric(horizontal: 50),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xFF464646)
            ),
            child: GestureDetector(
              onTap: ()  async {
                await FirebaseAuth.instance.signOut();
                final GoogleSignIn googleSignIn = GoogleSignIn();
                await googleSignIn.signOut();

                Navigator.pushNamedAndRemoveUntil(context, "/environment",ModalRoute.withName('/'));

              },
              child: Center(
                child: Text("Cerrar Sesion", style: TextStyle(fontSize: 18,color: Color(0xFFcccccf), fontWeight: FontWeight.bold),),
              ),
            )
        ),
      ),

    ])),);
  }
  
}