import 'package:flutter/material.dart';
import 'package:management_location_mobile/pages/home_page.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:management_location_mobile/pages/home_page_worker.dart';
import 'package:management_location_mobile/pages/portfolio_my_publication.dart';
import 'package:management_location_mobile/pages/portfolio_publication.dart';
import 'package:management_location_mobile/pages/portfolio_worker.dart';
import 'package:management_location_mobile/pages/professional_portfolio.dart';
import 'package:management_location_mobile/pages/profile_detail.dart';
import 'package:management_location_mobile/pages/profile_page.dart';
import 'package:management_location_mobile/provider/SelectMenuProvider.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:provider/provider.dart';

class TabBarHomePageCustomer extends StatefulWidget {
  Widget currentPage = HomePageWorker();
  String currentTitle = 'Home';

  int currentTab = 0;


  @override
  TabBarHomePageCustomerState createState() => TabBarHomePageCustomerState();
}

class TabBarHomePageCustomerState extends State<TabBarHomePageCustomer>  with TickerProviderStateMixin{

  late TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
    _tabController.animateTo(2);

  
  }

  void _selectTab(int tabItem) async {
    var user = await Util.getUser();
    setState(() {
      widget.currentTab = tabItem;
      switch (tabItem) {
        case 0:
          widget.currentTitle = AppLocalizations.of(context)!.portfolio_worker;
          widget.currentPage = HomePageWorker();
          break;
        case 1:
          widget.currentTitle = AppLocalizations.of(context)!.portfolio_worker;
          widget.currentPage = PortfolioWorker();

          break;
        case 2:
          widget.currentTitle = AppLocalizations.of(context)!.my_publications;
          widget.currentPage = PortfolioMyPublication();
          break;
        case 3:
          widget.currentTitle = AppLocalizations.of(context)!.portfolio_worker;
          widget.currentPage = ProfileDetailPage(user: user!);
          break;

      }
    });
  }

  Widget build(BuildContext context) {
    SelectMenuProvider counter = Provider.of<SelectMenuProvider>(context, listen: true);
    counter.addListener(() {
      _selectTab(counter.currentTab);
    });
    return Scaffold(
          appBar: AppBar(
          title: Text(widget.currentTitle,style: const TextStyle(color: Colors.white),),
          backgroundColor: Theme.of(context).accentColor,



        ),
        bottomNavigationBar:BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          selectedItemColor: Theme.of(context).focusColor,
          selectedFontSize: 0,
          unselectedFontSize: 0,
          iconSize: 22,
          elevation: 0,
          backgroundColor: Colors.transparent,
          selectedIconTheme: IconThemeData(size: 28),
          unselectedItemColor: Theme.of(context).hintColor.withOpacity(1),
          currentIndex: widget.currentTab,
          onTap: (int i) {
            this._selectTab(i);
          },
          // this will be set when a new tab is tapped
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.notifications),
              label: ""
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.timer),
                label: ""
            ),

            BottomNavigationBarItem(
              icon: new Icon(Icons.work),
                label: ""
            ),
            BottomNavigationBarItem(
              icon: new Icon(Icons.supervised_user_circle),
                label: ""
            ),
          ],
        ),
        body: widget.currentPage,


    );
  }

}