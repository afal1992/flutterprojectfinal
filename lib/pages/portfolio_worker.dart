import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/PaginationMetadata.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/model/order.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/PublicationItemWidget.dart';
import 'package:management_location_mobile/widgets/SearchBarWidget.dart';
import 'package:management_location_mobile/widgets/WorkerItemWidget.dart';

class PortfolioWorker extends StatefulWidget {
  const PortfolioWorker({Key? key}) : super(key: key);

  @override
  StatePortfolioWorker createState() => StatePortfolioWorker();
}

class StatePortfolioWorker extends State<PortfolioWorker> {
  List<Customer> customerArray = [];
  PaginationMetadata? paginationMetadata;
  static const _pageSize = 20;

  final PagingController<int, Publication> _pagingController =
  PagingController(firstPageKey: 1);


  @override
  void initState() {
    getWorkerArray();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
          children: <Widget>[
            SizedBox(height: 10,),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SearchBarWidget(),
            ),
      Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, right: 20),child:ListView.separated(
      padding: EdgeInsets.all(0),
      itemBuilder: (context, index) {
        return WorkerItemWidget(user: customerArray[index],showCat: true,);
      },
      separatorBuilder: (context, index) {
        return SizedBox(height: 20);
      },
      itemCount: customerArray.length,
      primary: false,
      shrinkWrap: true,
    ))],
    )
    ,
    );
  }

  void getWorkerArray() async {
    try {
      var responsePagination = await FetchServer.fetchPost(
          "worker/getPortfolioWorker", {});
      if (responsePagination.error ||
          responsePagination.responseRequest == null) {
        Util.showGenericError(context, () => {});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(
          responsePagination.responseRequest.toString());
      final List<dynamic> data = responseRequest['data'];

      List<Customer> _customerArray = (data)
          .map((i) => Customer.fromJson(i))
          .toList();

      setState(() {
        customerArray = _customerArray;
      });
    } catch (e) {
      print(e);
      Util.showGenericError(context, () => {});
    }
  }

  bool isLastPage(PaginationMetadata paginationMetadata) {
    return paginationMetadata.currentPage == paginationMetadata.totalPages;
  }
}
