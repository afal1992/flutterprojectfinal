import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/PaginationMetadata.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/order.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/PublicationItemWidget.dart';
import 'package:management_location_mobile/widgets/SearchBarWidget.dart';

class PortfolioPublication extends StatefulWidget {
  const PortfolioPublication({Key? key}) : super(key: key);

  @override
  StatePortfolioPublication createState() => StatePortfolioPublication();
}

class StatePortfolioPublication extends State<PortfolioPublication> {
  OrdersList _ordersList = new OrdersList();
  List<Publication> publicationArray = [];
  PaginationMetadata? paginationMetadata;
  static const _pageSize = 20;

  final PagingController<int, Publication> _pagingController =
  PagingController(firstPageKey: 1);


  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      getPublicationArray(pageKey);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
        children: <Widget>[
          SizedBox(height: 10,),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: SearchBarWidget(),
            ),
          SizedBox(height: 10),
          Expanded(child: PagedListView<int, Publication>(
            pagingController: _pagingController,
            builderDelegate: PagedChildBuilderDelegate<Publication>(
                itemBuilder: (context, item, index) => PublicationItemWidget(
                    heroTag: 'my_orders',
                    publication: item)
            ),
          ))
        ],
      ),
    );
  }

  void getPublicationArray(int pageKey) async {
    try {
      var dataPagination = {"page": pageKey, "limit": _pageSize};
      var responsePagination = await FetchServer.fetchPost("worker/getPublicationArray",dataPagination);
      if(responsePagination.error || responsePagination.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responsePagination.responseRequest.toString());
      final Map<String,dynamic> data = responseRequest['data'];
      final List<dynamic> publicationArrayResp = data['items'];
      final Map<String,dynamic> paginationResp = data['meta'];

      List<Publication> publicationArray = (publicationArrayResp)
          .map((i) => Publication.fromJson(i))
          .toList();
      PaginationMetadata _paginationMetadata = PaginationMetadata.fromJson(paginationResp);


      setState(() {
        this.publicationArray = publicationArray;
        paginationMetadata = _paginationMetadata;
      });

      if(isLastPage(_paginationMetadata)){
        _pagingController.appendLastPage(publicationArray);
      }else{
        final nextPageKey = _paginationMetadata.currentPage + 1;
        _pagingController.appendPage(publicationArray, nextPageKey);
      }




    } catch (e) {
      print(e);
      Util.showGenericError(context, () => {});
    }
  }

  bool isLastPage(PaginationMetadata paginationMetadata) {

    return paginationMetadata.currentPage == paginationMetadata.totalPages;

  }
}
