import 'dart:async';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/Priority.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/Validity.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_place/google_place.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:management_location_mobile/pages/categories_select_page.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../model/CategoryExperience.dart';
const kGoogleApiKey = "AIzaSyDjG8LJUzDgX4aBm7U-tG1T4VsBpvjVkWk";

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
final publicacioncaffoldKey = GlobalKey<ScaffoldState>();

class NewPublication extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return NewPublicationState();
  }
}

class NewPublicationState extends State<NewPublication> {

  final dataKey = new GlobalKey();
  final descKey = new GlobalKey();
  final prioridadKey = new GlobalKey();
  final vigenciadKey = new GlobalKey();
  Publication publication = new Publication();

 TextEditingController txtTitle = new TextEditingController();
  TextEditingController txtDescripcion = TextEditingController();
  bool validNombres = false;
  bool isLoading = false;
  String textLoading = "";
  CategoryExperience? especialidadSeleccionada;
  late List<PriorityPublication> prioritys;
  Completer<GoogleMapController> _controller = Completer();
  GooglePlace? googlePlace;
  List<AutocompletePrediction> predictions = [];
  TextEditingController txtSearch = TextEditingController();

  List<PriorityPublication> priorityArray = <PriorityPublication>[PriorityPublication('low','Baja',''), PriorityPublication('normal','Normal',''),
    PriorityPublication('high','Urgente','')];

  List<ValidityPublication> validityArray = <ValidityPublication>[new ValidityPublication('6','6 Horas','HOUR'), new ValidityPublication('12','12 Horas','HOUR'),
    new ValidityPublication('24','24 Horas','HOUR')];

  LatLng currentPosition = new LatLng(-2.9005499, -79.0045319);

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};


  String descripcion = "";

  @override
  void initState() {

    googlePlace = GooglePlace(kGoogleApiKey);
    super.initState();

  }




  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
        key: publicacioncaffoldKey,

        appBar: AppBar(
            actions: <Widget>[
              IconButton(
                color: Colors.white,
                icon: Icon(Icons.check),
                onPressed: () {
                  savePublication();
                },
              ),
            ],
            backgroundColor: Theme.of(context).accentColor,
            elevation: 0,
            centerTitle: false,
            title: Text(
              "Nueva Publicacion",
              style: Theme.of(context)
                  .textTheme.subtitle1!
                  .merge(TextStyle(letterSpacing: 1.3,color: Colors.white)),
            )),
        body:LoadingOverlay(
            child: SingleChildScrollView(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      TextField(
                        style: TextStyle(color: config.Colors().secondColor(1)),
                        keyboardType: TextInputType.text,
                        controller: txtTitle,
                        maxLength: 500,
                        maxLines: 1,
                        decoration: InputDecoration(
                          hintText: "Titulo",
                          labelStyle:
                          TextStyle(color: Theme.of(context).accentColor),
                          contentPadding: EdgeInsets.all(12),
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.7)),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                          errorText: validNombres ? 'Campo Requerido' : null,
                        ),
                      ),
                      TextField(
                        style: TextStyle(color: config.Colors().secondColor(1)),
                        key:descKey,
                        keyboardType: TextInputType.text,
                        controller: txtDescripcion,
                        maxLength: 500,
                        maxLines: 7,
                        decoration: InputDecoration(
                          hintText: descripcion != null && !descripcion.isEmpty ? descripcion : "Describe tu problema",
                          labelStyle:
                              TextStyle(color: Theme.of(context).accentColor),
                          contentPadding: EdgeInsets.all(12),
                          hintStyle: TextStyle(
                              color: Theme.of(context)
                                  .focusColor
                                  .withOpacity(0.7)),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.5))),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Theme.of(context)
                                      .focusColor
                                      .withOpacity(0.2))),
                          errorText: validNombres ? 'Campo Requerido' : null,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),




                      SizedBox(height: 10,),
                      DropdownButton<String>(
                        key: prioridadKey,


                        hint: Text("Prioridad",style:TextStyle(
                            color: Theme.of(context)
                                .focusColor
                                .withOpacity(0.7)),),
                        isExpanded: true,
                        value: publication.priority,
                        onChanged: (String? newValue) {
                          setState(() {
                            publication.priority = newValue;
                          });
                        },
                        items: priorityArray.map((item) {
                          return DropdownMenuItem<String>(
                            value: item.id,

                            child: Text(
                              item.label,
                                style: TextStyle(color: config.Colors().secondColor(1)),

                            ),
                          );
                        }).toList(),
                      ),


                      SizedBox(height: 20,),

                       DropdownButton<String>(
                         value: publication.validateTime,

                         key:vigenciadKey,
                        hint: Text("Periodo de validez",style:TextStyle(
                        color: Theme.of(context)
                            .focusColor
                            .withOpacity(0.7)),),
                        isExpanded: true,
                        onChanged: (String? newValue) {
                          setState(() {
                            publication.validateTime = newValue;
                          });
                        },
                        items: validityArray.map((item) {
                          return DropdownMenuItem<String>(
                            value: item.key,
                            child: Text(
                                item.value,
                              style: TextStyle(color: config.Colors().secondColor(1)),

                            ),
                          );
                        }).toList(),
                      ),

                      SizedBox(height: 20,),



                      Container(
                          margin:
                          EdgeInsets.symmetric(horizontal: 1, vertical: 10),
                          decoration: BoxDecoration(
                            color: Theme.of(context).primaryColor,
                            borderRadius: BorderRadius.circular(6),
                            boxShadow: [
                              BoxShadow(
                                  color: Theme.of(context)
                                      .hintColor
                                      .withOpacity(0.15),
                                  offset: Offset(0, 3),
                                  blurRadius: 10)
                            ],
                          ),
                          child:ListView(
                              padding: EdgeInsets.all(5.0),
                              shrinkWrap: true,
                              primary: false,
                              children: <Widget>[
                                itemTitulo("Categorias", Icons.gavel, 0, true,
                                    // ignore: sdk_version_set_literal
                                    pAccion: () =>
                                    {
                                      selectCategories()


                                    }),

                                publication.categoryExperience!=null?itemEspecialidad(publication.categoryExperience):new Text("'"),
                              ])),


                      FocusScope(
                          child: Focus(
                              onFocusChange: (focus) => {
                                Scrollable.ensureVisible(dataKey.currentContext!),
                              },
                              child: TextField(
                                key: dataKey,
                                keyboardType: TextInputType.text,
                                maxLines: 1,
                                style: TextStyle(color: config.Colors().secondColor(1)),

                                controller: txtSearch,
                                decoration: InputDecoration(
                                  hintText: "Selecciona tu ubicacion.",
                                  labelStyle:
                                  TextStyle(color: Theme.of(context).accentColor),
                                  contentPadding: EdgeInsets.all(12),
                                  hintStyle: TextStyle(
                                      color: Theme.of(context)
                                          .focusColor
                                          .withOpacity(0.7)),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.5))),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Theme.of(context)
                                              .focusColor
                                              .withOpacity(0.2))),
                                ),
                                onChanged: (value) {
                                  if (value.isNotEmpty) {
                                    autoCompleteSearch(value);
                                  } else {
                                    if (predictions.length > 0 && mounted) {
                                      setState(() {
                                        predictions = [];
                                      });
                                    }
                                  }
                                },
                              ))),

                      Visibility(visible:predictions!=null&&predictions.isNotEmpty?true:false,child:
                      SizedBox(
                        width: MediaQuery.of(context).size.width,  // or use fixed size like 200
                        height: 200,
                        child: ListView.builder(
                          itemCount: predictions.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              leading: Icon(
                                Icons.pin_drop,
                                color: config.Colors().secondColor(1),
                              ),

                              title: Text(predictions[index].description ?? "",
                                  style: TextStyle(color: config.Colors().secondColor(1)),
                            ),
                              onTap: () {
                                displayPrediction(predictions[index].placeId ?? "",predictions[index].description ?? "");

                              },
                            );
                          },
                        ),
                      )),

                      SizedBox(height: 20,),

                      SizedBox(
                          width: MediaQuery.of(context).size.width,  // or use fixed size like 200
                          height: 350,
                          child: Stack(
                              children: <Widget>[GoogleMap(
                                gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                                  new Factory<OneSequenceGestureRecognizer>(
                                        () => new EagerGestureRecognizer(),
                                  ),
                                ].toSet(),
                                mapType: MapType.normal,
                                initialCameraPosition: CameraPosition(
                                  target: LatLng(
                                      currentPosition.latitude, currentPosition.longitude),
                                  zoom: 11.0,


                                ),

                                onMapCreated: (GoogleMapController controller) {
                                  _controller.complete(controller);
                                  getCurrentPosition();
                                },onCameraMove: (CameraPosition position) {
                                currentPosition = position.target;
                              },
                                markers: Set<Marker>.of(markers.values),

                              ),pin()])),





                    ])),
            isLoading: isLoading,
            // demo of some additional parameters
            opacity: 0.8,
            color: Theme.of(context).accentColor,
            progressIndicator: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(),
                  SizedBox(
                    height: 10,
                  ),
                  new Text(
                    textLoading,
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ],
              ),
            )));
  }

  Widget pin() {
    return IgnorePointer(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.place, size: 56),
            Container(
              decoration: ShapeDecoration(
                shadows: [
                  BoxShadow(
                    blurRadius: 4,
                    color: Colors.black38,
                  ),
                ],
                shape: CircleBorder(
                  side: BorderSide(
                    width: 4,
                    color: Colors.transparent,
                  ),
                ),
              ),
            ),
            SizedBox(height: 56),
          ],
        ),
      ),
    );
  }




  Widget itemEspecialidad(var especialidades) {
    if (especialidades == null) {
      return Container();
    }
    List<Widget> wid_especialidades = [];
    for (var place in especialidades) {
      wid_especialidades
          .add(chipEspecialidades(place.name));
    }
    return wid_especialidades == null || wid_especialidades.length == 0
        ? Text("")
        : Wrap(
      spacing: 6.0,
      runSpacing: 6.0,
      children: wid_especialidades,
    );
  }
  Widget chipEspecialidades(String label) {
    return Chip(
      labelPadding: EdgeInsets.all(4.0),
      avatar: CircleAvatar(
        backgroundColor: Colors.blueGrey[300],
        child: Text(
          label[0].toUpperCase(),
          style: TextStyle(color: Colors.white),
        ),
      ),
      label: Text(
        label,
      ),
      backgroundColor: Colors.grey.shade800,
      shadowColor: Colors.grey[60],
      padding: EdgeInsets.all(1.0),
    );
  }

  Widget itemTitulo(String titulo, var icon, int op, bool showaccion,
      {required Function pAccion}) {
    return ListTile(

      title: Text(
        titulo,
        style: Theme.of(context).textTheme.bodyText2,
      ),
      trailing: showaccion == false
          ? Text("")
          : ButtonTheme(
        padding: EdgeInsets.all(0),
        minWidth: 50.0,
        height: 25.0,
        child: ClipOval(
          child: Material(
            color: Colors.blueGrey, // button color
            child: InkWell(
              splashColor: Colors.blue, // inkwell color
              child: SizedBox(
                  width: 40,
                  height: 40,
                  child: Icon(
                     Icons.add ,
                    color: Colors.white,
                  )),
              onTap: () {
                pAccion();
              },
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getCurrentPosition() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    if(position == null){
      return;
    }

    currentPosition = LatLng(position.latitude, position.longitude);
    LatLng positionOrigin = new LatLng(-2.90055, -79.00453);
    if(currentPosition != null){

      positionOrigin = new LatLng(currentPosition.latitude, currentPosition.longitude);

    }

    addMarkerOrigin(positionOrigin,"currentposition","Mi Ubicación");

  }

  addMarkerOrigin(LatLng latLng, String id, String leyend) async {

    try {
      setState(() {
        currentPositionCamera(latLng);
      });
    }catch(e){
      print(e);
    }
  }

  Future<void> currentPositionCamera(LatLng marker) async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(new CameraPosition(
      target: LatLng(marker.latitude, marker.longitude),
      zoom: 14.0,
    )));
  }

  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: kGoogleApiKey);
  final customTheme = ThemeData(
    primarySwatch: Colors.blue,
    brightness: Brightness.dark,
    accentColor: Colors.redAccent,
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.all(Radius.circular(4.00)),
      ),
      contentPadding: EdgeInsets.symmetric(
        vertical: 12.50,
        horizontal: 10.00,
      ),
    ),
  );
  Mode _mode = Mode.fullscreen;
  void onError(PlacesAutocompleteResponse response) {
    publicacioncaffoldKey.currentState?.showSnackBar(
      SnackBar(content: Text(response.errorMessage ?? "")),
    );
  }

  Future<void> _handlePressButton() async {
    // show input autocomplete with selected mode
    // then get the Prediction selected
    Prediction? p = await PlacesAutocomplete.show(
      context: context,
      apiKey: kGoogleApiKey,
      onError: onError,
      mode: _mode,
    );

    //isplayPrediction(p, publicacioncaffoldKey.currentState);
  }

  Future<Null> displayPrediction(String placeId,String description) async {
    if (placeId != null) {
      // get detail (lat/lng)
      PlacesDetailsResponse detail =
      await _places.getDetailsByPlaceId(placeId);
      final lat = detail.result.geometry?.location.lat;
      final lng = detail.result.geometry?.location.lng;
      if(lat == null || lng == null) return;
      LatLng position =new LatLng(lat,lng);
      addMarkerOrigin(position, "currentposition", "Mi posición actual.");
      predictions = [];
      txtSearch.text = description;
      FocusScope.of(context).requestFocus(new FocusNode());


    }
  }
  void autoCompleteSearch(String value) async {
    var result = await googlePlace?.autocomplete.get(value);
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions!;
      });
    }
  }


  void selectCategories() async {
    var response = await (Navigator.push(context,
      MaterialPageRoute(builder: (context) => CategoriesSelectPage()),));
    if (response == null) return;

    List<CategoryExperience> categoryArraySelect = response as List<
        CategoryExperience>;
    if (publication.categoryExperience !=null ) {
      var filter = publication.categoryExperience!.map((e) => e.id);
      if (filter != null) {
        categoryArraySelect.retainWhere((element) =>
        !filter.contains(
            element.id));
      }
    }
      setState(() {
        if(publication.categoryExperience == null) publication.categoryExperience = [];
        publication.categoryExperience!.addAll(categoryArraySelect);
      });

  }

  void savePublication() async {
    try{

      final createDate = DateTime.now();
      int hours = int.parse(publication.validateTime ?? "1");
      final endDate = createDate.add(Duration(hours: hours));

      publication.createDate = createDate;
      publication.endDate = endDate;


      publication.title = txtTitle.text;
      publication.description = txtDescripcion.text;
      publication.latitude = currentPosition.latitude;
      publication.longitude = currentPosition.longitude;

      publication.state = "active";


      var responseRequest = await FetchServer.fetchPost("worker/savePublication", publication.toJson());

      if(responseRequest.error || responseRequest.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }


      final Map<String, dynamic> responseData = jsonDecode(responseRequest.responseRequest.toString());
      var error = responseData['error'] ;
      String title = AppLocalizations.of(context)!.postulation;
      if(error == null || error.toString().isEmpty || error.toString().toLowerCase() != "false"){
        Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.savePublicationError);
        return;
      }
      Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.savePublicationSuccess);


    }catch(e){
      print(e);
      Util.showGenericError(context, null);

    }

  }





}
class User {
  User(this.name);

   String name;
}
