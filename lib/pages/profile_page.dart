import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/pages/profile_cateegory.dart';
import 'package:management_location_mobile/pages/profile_contact.dart';
import 'package:management_location_mobile/pages/profile_detail.dart';
import 'package:management_location_mobile/util/util.dart';

class ProfilePage extends StatefulWidget{
  const ProfilePage({Key? key}) : super(key: key);

  @override
  StateProfile createState() => StateProfile();

}
class StateProfile extends State<ProfilePage>{

  @override
  void initState() {
    this.getProfileUser();
    super.initState();
  }

  Customer? user;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return Scaffold(body:DefaultTabController(
     length: 3,
     child: Scaffold(
       appBar: AppBar(
         backgroundColor: Theme.of(context).accentColor,

         bottom:  TabBar(
           tabs: [
             Tab(icon: Icon(Icons.people,color: Theme.of(context).focusColor,)),
             Tab(icon: Icon(Icons.phone,color: Theme.of(context).focusColor)),
             Tab(icon: Icon(Icons.work,color: Theme.of(context).focusColor)),
           ],
         ),
         title: Text(AppLocalizations.of(context)!.profile_user,style: const TextStyle(color: Colors.white),),
       ),
       body: TabBarView(
         children: [
           user != null ? ProfileDetailPage(user: user!): new Container(),
           user != null ? ProfileContactPage(user: user!): new Container(),
           user != null ? ProfileCategoryPage(user: user!): new Container(),

         ],
       ),
     ),
   ),
   );
  }


  void getProfileUser() async {
    try{
      
      var responseProfile = await FetchServer.fetchPost("worker/getProfile", {});
      if(responseProfile.error || responseProfile.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responseProfile.responseRequest.toString());
      final Map<String,dynamic>   data = responseRequest['data'];

      setState(() {
        user = Customer.fromJson(data);
      });

    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});
    }
  }

}