import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter/gestures.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/model/postulation.dart';
import 'package:management_location_mobile/pages/enviroment_select.dart';
import 'package:management_location_mobile/pages/save_postulation.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/WorkerItemWidget.dart';

class DetailPublication extends StatefulWidget{

   final Publication publication;
  const DetailPublication({Key? key,required this.publication}) : super(key: key);

  @override
  StateDetailPublication createState() => StateDetailPublication();

}

class StateDetailPublication extends State<DetailPublication> {
  final Completer<GoogleMapController> _controllerGoogle = Completer();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  bool isLoading = false;

  @override
  void initState() {
    getPostulationArray();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).accentColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.detailPublication,
          style: Theme.of(context)
              .textTheme.subtitle1!
              .merge(TextStyle(letterSpacing: 1.3,color:Colors.white)),
        ),
      ),
      body: LoadingOverlay(
          child: this.widget.publication == null
              ? new Container()
              : SingleChildScrollView(
            child: Column(
              children: <Widget>[

            Container(
            margin: const EdgeInsets.symmetric(
                horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: BorderRadius.circular(6),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context)
                        .hintColor
                        .withOpacity(0.15),
                    offset: Offset(0, 3),
                    blurRadius: 10)
              ],
            ),
            child:ListView(
              padding: EdgeInsets.all(5.0),
              shrinkWrap: true,
              primary: false,
              children: [itemTitulo(AppLocalizations.of(context)!.title, Icons.title,
              0, false,
              // ignore: sdk_version_set_literal
            ),Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                this
                    .widget.publication
                    .title ?? '',
                textAlign: TextAlign.justify,
              ),
            )],),),

                Container(
                    margin: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                      color: Theme.of(context).accentColor,
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                        BoxShadow(
                            color: Theme.of(context)
                                .hintColor
                                .withOpacity(0.15),
                            offset: Offset(0, 3),
                            blurRadius: 10)
                      ],
                    ),
                    child: ListView(
                        padding: EdgeInsets.all(5.0),
                        shrinkWrap: true,
                        primary: false,
                        children: <Widget>[
                          itemTitulo("Descripcion", Icons.pending_actions,
                            0, false,
                            // ignore: sdk_version_set_literal
                          ),
                          Container(
                              padding: EdgeInsets.all(10.0),child:Text(
                            widget.publication.createDate != null? DateFormat("yyyy-MM-dd").format(this.widget.publication.createDate!):"",
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: Theme.of(context).textTheme.caption,
                          )),
                          this.widget.publication == null ||
                              this
                                  .widget.publication
                                  .description ==
                                  null
                              ? Container()
                              : Container(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              this
                                  .widget.publication
                                  .description ?? "",
                              textAlign: TextAlign.justify,
                            ),
                          ),
                        ])),


                Container(
                  margin: EdgeInsets.symmetric(
                      horizontal: 20, vertical: 10),
                  decoration: BoxDecoration(
                    color: Theme.of(context).accentColor,
                    borderRadius: BorderRadius.circular(6),
                    boxShadow: [
                      BoxShadow(
                          color: Theme.of(context)
                              .hintColor
                              .withOpacity(0.15),
                          offset: Offset(0, 3),
                          blurRadius: 10)
                    ],
                  ),
                  child:
                  Container(
                      padding: EdgeInsets.all(15.0),

                      child:
                      Row(

                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text('Prioridad'),

                              Text(
                                widget.publication.priority  ?? "",
                                style: TextStyle(fontWeight: FontWeight.w600),
                              ),
                            ],
                          ),
                          widget.publication.endDate != null ? Column(
                            children: <Widget>[
                              Icon(Icons.timer),
                              SizedBox(height: 5,),
                              Text(
                                "Caduca en: "+getDiffVigencia(widget.publication.endDate!)+" horas",
                                style: Theme.of(context).textTheme.caption,
                              ),

                            ],
                          ): new Container(),
                          Column(
                            children: <Widget>[
                              Text('Estado'),

                              Text(widget.publication.state ?? "")
                            ],
                          )
                        ],
                      )),
                ),
                this.postulationArray == null || postulationArray!.isEmpty ? new Container() : Container(
            margin: EdgeInsets.symmetric(
                horizontal: 20, vertical: 10),
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: BorderRadius.circular(6),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context)
                        .hintColor
                        .withOpacity(0.15),
                    offset: Offset(0, 3),
                    blurRadius: 10)
              ],
            ),
            child: Container(
              padding: EdgeInsets.all(15.0),

              child:Column(children: [Text(
                "Postulantes",
                style: Theme.of(context).textTheme.bodyText2,
              ),generatePostulationWidget()],))),


                Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(6),
                      boxShadow: [
                        BoxShadow(
                            color: Theme.of(context)
                                .hintColor
                                .withOpacity(0.15),
                            offset: Offset(0, 3),
                            blurRadius: 10)
                      ],
                    )),

                Container(
                    margin: EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10) ,child:SizedBox(

                    width: MediaQuery.of(context).size.width,  // or use fixed size like 200
                    height: 350,
                    child: GoogleMap(
                      gestureRecognizers: Set()
                        ..add(Factory<OneSequenceGestureRecognizer>(
                                () => EagerGestureRecognizer())),
                      mapType: MapType.normal,
                      initialCameraPosition: CameraPosition(
                        target: LatLng(
                            widget.publication.latitude  ?? 0, widget.publication.longitude  ?? 0),
                        zoom: 11.0,


                      ),
                      onMapCreated: (GoogleMapController controller) {
                        _controllerGoogle.complete(controller);
                      },
                      //markers: Set<Marker>.of(markers.values),

                    ))),

          Container(
              margin: EdgeInsets.symmetric(
                  horizontal: 5, vertical: 10),
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.circular(6),
                boxShadow: [
                  BoxShadow(
                      color: Theme.of(context)
                          .hintColor
                          .withOpacity(0.15),
                      offset: Offset(0, 3),
                      blurRadius: 10)
                ],
              ),
              child: Container(
                  padding: EdgeInsets.all(15.0),

                  child:Column(children: [
                    Text(
                      "Categorias",
                      style: Theme.of(context).textTheme.bodyText2,
                    ),
            SizedBox(height: 20,),
            itemCategories(widget.publication.categoryExperience)],))
          ),


              ],
            ),
          ),
          isLoading: isLoading,
          opacity: 0.8,
          color: Theme.of(context).accentColor,
          progressIndicator: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(
                  height: 10,
                ),
                new Text(
                  "",
                  style: Theme.of(context).textTheme.headline1,
                ),
              ],
            ),
          )),floatingActionButton: Container(
      padding: EdgeInsets.only(top: 100.0),
      child: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,MaterialPageRoute(builder: (context) =>  SavePostulation(publication: widget.publication,)),);

        },
        child: new IconTheme(
          data: new IconThemeData(color: Colors.white),
          child: new Icon(Icons.post_add),
        ),
        backgroundColor: Theme.of(context).focusColor,
      ),
    ),
    );
  }

  Widget itemCategories(var especialidades) {
    if (especialidades == null) {
      return Container();
    }
    List<Widget> wid_especialidades = [];
    for (var place in especialidades) {
      wid_especialidades
          .add(chipEspecialidades(place.name));
    }
    return wid_especialidades == null || wid_especialidades.length == 0
        ? Text("")
        : Wrap(
      spacing: 6.0,
      runSpacing: 6.0,
      children: wid_especialidades,
    );
  }
  Widget chipEspecialidades(String label) {
    return Chip(
      labelPadding: EdgeInsets.all(4.0),
      avatar: CircleAvatar(
        backgroundColor: Colors.blueGrey[300],
        child: Text(
          label[0].toUpperCase(),
          style: TextStyle(color: Colors.white),
        ),
      ),
      label: Text(
        label,
      ),
      backgroundColor: Colors.grey.shade800,
      shadowColor: Colors.grey[60],
      padding: EdgeInsets.all(1.0),
    );
  }

  String getDiffVigencia(DateTime fechaVigencia) {
    final now = DateTime.now();
    final difference = fechaVigencia.difference(now).inHours;
    return difference.toString();
  }

  Widget itemTitulo(String titulo, var icon, int op, bool showaccion,
      {Function? pAccion}) {
    return ListTile(
      leading: Icon(icon),
      title: Text(
        titulo,
        style: Theme.of(context).textTheme.bodyText2,
      ),
      trailing: showaccion == false
          ? Text("")
          : ButtonTheme(
        padding: EdgeInsets.all(0),
        minWidth: 50.0,
        height: 25.0,
        child: InkWell(
          splashColor: Colors.blue, // inkwell color
          child: Icon(
            Icons.chevron_right,
            color: Theme.of(context).accentColor,
          ),
          onTap: () {
            if(pAccion != null) pAccion();
          },
        ),
      ),
    );
  }

  List<Postulation>? postulationArray = [];
  void getPostulationArray () async {

    try{


      Customer? customer = await Util.getUser();
      if(customer == null || customer!.environment != EnvironmentSelect.ENV_CLIENT){
        return;
      }
      var dataR = {"idPublication": widget.publication.id};
      var responsePagination = await FetchServer.fetchPost("worker/getPostulationByPublication", dataR);
      if(responsePagination.error || responsePagination.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responsePagination.responseRequest.toString());
      final List<dynamic> data = responseRequest['data'];

      List<Postulation> _postulationArray = (data)
          .map((i) => Postulation.fromJson(i))
          .toList();

      setState((){
        this.postulationArray = _postulationArray;
      });


    }catch(e){
      print(e);
    }

  }

  Widget generatePostulationWidget () {

    if(this.postulationArray == null || postulationArray!.isEmpty){
      return new Container();
    }

    return Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),child:ListView.separated(
      padding: EdgeInsets.all(0),
      itemBuilder: (context, index) {
        return WorkerItemWidget(user: postulationArray![index].customer,showCat: false);
      },
      separatorBuilder: (context, index) {
        return SizedBox(height: 5);
      },
      itemCount: postulationArray!.length,
      primary: false,
      shrinkWrap: true,
    ));


  }
}