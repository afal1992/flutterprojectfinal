import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/PaginationMetadata.dart';
import 'package:management_location_mobile/model/Publication.dart';
import 'package:management_location_mobile/model/order.dart';
import 'package:management_location_mobile/pages/new_publication.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/PublicationItemWidget.dart';
import 'package:management_location_mobile/widgets/SearchBarWidget.dart';

class PortfolioMyPublication extends StatefulWidget {
  const PortfolioMyPublication({Key? key}) : super(key: key);

  @override
  StatePortfolioMyPublication createState() => StatePortfolioMyPublication();
}

class StatePortfolioMyPublication extends State<PortfolioMyPublication> {
  List<Publication> publicationArray = [];

  @override
  void initState() {
    getPublicationArray();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,children: <Widget>[
          SizedBox(height: 5,),

          Padding(
              padding: const EdgeInsets.only(top: 5, left: 5, right: 5),child:ListView.separated(
            padding: EdgeInsets.all(0),
            itemBuilder: (context, index) {
              return PublicationItemWidget(publication: publicationArray[index]);
            },
            separatorBuilder: (context, index) {
              return SizedBox(height: 1);
            },
            itemCount: publicationArray.length,
            primary: false,
            shrinkWrap: true,
          ))
        ],
      )),floatingActionButton: Container(
      padding: EdgeInsets.only(top: 100.0),
      child: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,MaterialPageRoute(builder: (context) =>  NewPublication()),);

        },
        child: new IconTheme(
          data: new IconThemeData(color: Colors.white),
          child: new Icon(Icons.post_add),
        ),
        backgroundColor: Theme.of(context).focusColor,
      ),
    ),
    );
  }

  void getPublicationArray() async {
    try {
      var responsePagination = await FetchServer.fetchPost("worker/getPublicationByUser",{});
      if(responsePagination.error || responsePagination.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responsePagination.responseRequest.toString());
      final List<dynamic> data = responseRequest['data'];

      List<Publication> publicationArray = (data)
          .map((i) => Publication.fromJson(i))
          .toList();


      setState(() {
        this.publicationArray = publicationArray;
      });


    } catch (e) {
      print(e);
      Util.showGenericError(context, () => {});
    }
  }

  bool isLastPage(PaginationMetadata paginationMetadata) {

    return paginationMetadata.currentPage == paginationMetadata.totalPages;

  }
}
