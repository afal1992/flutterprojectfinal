import 'package:flutter/material.dart';
import 'package:management_location_mobile/pages/login_page.dart';
import 'package:management_location_mobile/widgets/FadeAnimation.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;

class   EnvironmentSelect extends StatelessWidget{
  const EnvironmentSelect({Key? key}) : super(key: key);
  static const ENV_WORKER = "WORKER";
  static const ENV_CLIENT = "CLIENT";

  @override
  Widget build(BuildContext context) {
    return SafeArea(

      child: Scaffold(
        body:SingleChildScrollView(
    child: Column(

    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[Container(

            width: double.infinity,

            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Column(

                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipPath(
                          clipper: WaveClipperTwo(),

                          child: Container(
                            width: 300.0,
                            height: 40.0,
                            decoration: const BoxDecoration(

                              gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Color(0xFF00802b),
                                  Color(0x0000722e),
                                ],
                              ),
                            ),
                          ),
                        ),


                      ],
                    ),

                    const SizedBox(height: 50,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Column(children:[Container(
                        width:80,height:80,child:const Image(image:
                          AssetImage("assets/images/teamwork.png"),
                      )),
                        SizedBox(height: 10,),Text(
                        "TripLi",
                        style: Theme.of(context).textTheme.headline2,
                      )]),

                    ),
                    SizedBox(height: 50),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child: FadeAnimation(1.6,Text(
                        AppLocalizations.of(context)!.enviroment_title,
                        maxLines: 4,

                        style: const TextStyle(
                            color: Color(0xFFcccccf),
                            fontWeight: FontWeight.bold,
                            fontSize: 15

                        ),
                      )),
                    ),

                    SizedBox(height: 20),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child: FadeAnimation(1.6,Text(
                        AppLocalizations.of(context)!.enviroment_subtitle,
                        maxLines: 10,

                        style: const TextStyle(
                            color: Color(0xFF777779),
                            fontWeight: FontWeight.bold,
                            fontSize: 13

                        ),
                      )),
                    ),
                    const SizedBox(height: 80),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child: FadeAnimation(1.6, Container(
                          height: 50,
                          width: 250,

                          // margin: EdgeInsets.symmetric(horizontal: 50),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20),
                              color: Color(0xFF464646)
                          ),
                          child: GestureDetector(
                            onTap: () {
                              Navigator.push(context,MaterialPageRoute(builder: (context) => const LoginPage(environment: ENV_WORKER,)),);
                            },
                            child: Center(
                              child: Text(AppLocalizations.of(context)!.enviroment_worker, style: TextStyle(fontSize: 18,color: Color(0xFFcccccf), fontWeight: FontWeight.bold),),
                            ),
                          )
                      )),
                    ),
                    const SizedBox(height: 30,),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(context,MaterialPageRoute(builder: (context) => const LoginPage(environment: ENV_CLIENT,)),);
                      },
                      child:Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35),
                      child: FadeAnimation(1.6, Container(
                          height: 50,
                          width: 250,

                          // margin: EdgeInsets.symmetric(horizontal: 50),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: const LinearGradient(
                                colors: [
                                  Color(0xFF00802b),
                                  Color(0x00722e),
                                ]
                            ),
                          ),
                          child:  Center(
                              child: Text(AppLocalizations.of(context)!.enviroment_client,
                                   style: TextStyle(fontSize: 17,color:Colors.white, fontWeight: FontWeight.bold),),
                            ),
                          )
                      )),
                    ),


                    const SizedBox(height: 30,),



                  ],
                ),
              ],

            )

        )])),
      ),
    );
  }

}
