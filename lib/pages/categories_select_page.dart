import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/util/util.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import '../model/CategoryExperience.dart';

class CategoriesSelectPage extends StatefulWidget{
  @override
  StateCategoriesSelectPage createState() => StateCategoriesSelectPage();
}
class StateCategoriesSelectPage extends State<CategoriesSelectPage>{

  List<CategoryExperience> categoryList = [];

  @override
  void initState() {
    this.getCategoryArray();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(appBar:
    AppBar(
      actions: <Widget>[
        IconButton(
          color: Theme.of(context)
              .focusColor
              .withOpacity(1),
          icon: Icon(Icons.check),
          onPressed: () {
           this.selectCategories();
          },
        ),
      ],
      backgroundColor: Theme.of(context).accentColor,
      elevation: 0,
      centerTitle: true,
      title: Text(
        AppLocalizations.of(context)!.select_category,
        style: Theme.of(context)
            .textTheme.subtitle1!
            .merge(TextStyle(letterSpacing: 1.3,color:Colors.white)),
      ),
    ),body:SingleChildScrollView(
    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
    child: Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[

    categoryList!=null?ListView.separated(
    padding: EdgeInsets.all(0),
      itemBuilder: (context, index) {
        return InkWell(
          child: Wrap(
            direction: Axis.horizontal,
            runSpacing: 10,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 65,
                    width: 65,
                    child: Checkbox(
                      value: categoryList[index].select,
                      onChanged: (bool? value) {
                        setState(() {
                          categoryList[index].select = value!;
                        });
                      },
                    ), //
                  ),
                  SizedBox(width: 15),
                  Flexible(
                    child: Text(
                      categoryList[index].name,
                      overflow: TextOverflow.fade,
                      softWrap: false,
                      maxLines: 2,
                      style: Theme.of(context).textTheme.subtitle1!.merge(
                          TextStyle(color: Theme.of(context).hintColor)),
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      },
      separatorBuilder: (context, index) {
        return SizedBox(height: 20);
      },
      itemCount: categoryList.length,
      primary: false,
      shrinkWrap: true,
    ): new Container()

    ])));

  }

  void getCategoryArray() async {
    try {

      var responseCategory = await FetchServer.fetchPost("worker/getCategoryArray",{});
      if(responseCategory.error || responseCategory.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }
      final Map<String, dynamic> responseRequest = jsonDecode(responseCategory.responseRequest.toString());
      final List<dynamic> data = responseRequest['data'];

      List<CategoryExperience> _categorieList = (data)
          .map((i) => CategoryExperience.fromJson(i))
          .toList();


      setState(() {
        categoryList = _categorieList;
      });






    } catch (e) {
      print(e);
      Util.showGenericError(context, () => {});
    }
  }

  void selectCategories () {

    if(this.categoryList == null || this.categoryList.isEmpty) return;

    categoryList.retainWhere((element) => element.select);

    Navigator.pop(context, categoryList);


  }
  
}