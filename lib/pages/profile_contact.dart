import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:management_location_mobile/connection/fetch_server.dart';
import 'package:management_location_mobile/model/customer.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:management_location_mobile/util/util.dart';
import 'package:management_location_mobile/widgets/FadeAnimation.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class ProfileContactPage extends StatefulWidget{
  final Customer user;
  const ProfileContactPage({Key? key,required this.user}) : super(key: key);

  @override
  StateProfileContactPage createState() => StateProfileContactPage();

}
class StateProfileContactPage extends State<ProfileContactPage>{
  final _formKey = GlobalKey<FormState>();

  TextEditingController txtPhone = new TextEditingController();
  TextEditingController txtdirection = new TextEditingController();
  TextEditingController txtFacebook = new TextEditingController();
  TextEditingController txtLinkedIn = new TextEditingController();
  TextEditingController txtInstagram = new TextEditingController();

  @override
  void initState() {


    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    this.txtPhone.text = widget.user.phone ?? "";
    this.txtdirection.text = widget.user.direction ?? "";
    this.txtFacebook.text = widget.user.facebook ?? "";
    this.txtLinkedIn.text = widget.user.linkedin ?? "";
    this.txtInstagram.text = widget.user.instagram ?? "";

    return Scaffold(body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    child: Form(
    key: _formKey,
    child:Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[


      SizedBox(height: 20,),

      TextFormField(
        controller: txtPhone,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return AppLocalizations.of(context)!.requiredField;
          }
          return null;
        },
        style: TextStyle(color: config.Colors().secondColor(1)),
        keyboardType: TextInputType.text,
        maxLines: 1,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.phone),
            labelText: "Telefono",
          labelStyle:
          TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
          contentPadding: EdgeInsets.all(12),
          hintStyle: TextStyle(
              color: Theme.of(context)
                  .focusColor
                  .withOpacity(0.7)),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.5))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
        ),
      ),
      SizedBox(height: 20,),

      TextFormField(
        controller: txtdirection,

        validator: (value) {
          if (value == null || value.isEmpty) {
            return AppLocalizations.of(context)!.requiredField;
          }
          return null;
        },
        keyboardType: TextInputType.text,
        maxLines: 3,
        style: TextStyle(color: config.Colors().secondColor(1)),

        decoration: InputDecoration(
          labelText: "Dirección",
          prefixIcon: Icon(Icons.people),

          labelStyle:
          TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
          contentPadding: EdgeInsets.all(12),
          hintStyle: TextStyle(
              color: Theme.of(context)
                  .focusColor
                  .withOpacity(0.7)),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.5))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
        ),
      ),
      SizedBox(height: 20,),

      TextFormField(
        controller: txtFacebook,

        keyboardType: TextInputType.text,
        maxLines: 1,
        style: TextStyle(color: config.Colors().secondColor(1)),

        decoration: InputDecoration(
          labelText: "Perfil Facebook",
          prefixIcon: Icon(Icons.facebook),

          labelStyle:
          TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
          contentPadding: EdgeInsets.all(12),
          hintStyle: TextStyle(
              color: Theme.of(context)
                  .focusColor
                  .withOpacity(0.7)),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.5))),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.2))),
        ),
      )
        ,
      SizedBox(height: 20,),

      TextFormField(
        controller: txtLinkedIn,

        keyboardType: TextInputType.text,
    maxLines: 1,
        style: TextStyle(color: config.Colors().secondColor(1)),

    decoration: InputDecoration(
    labelText: "Perfil LinkedIn",
    prefixIcon: Icon(Icons.work),

    labelStyle:
    TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
    contentPadding: EdgeInsets.all(12),
    hintStyle: TextStyle(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.7)),
    border: OutlineInputBorder(
    borderSide: BorderSide(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.2))),
    focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.5))),
    enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.2))),
    ),
    ),
      SizedBox(height: 20,),

      TextFormField(
        controller: txtInstagram,

        keyboardType: TextInputType.text,
    maxLines: 1,
    style: TextStyle(color: config.Colors().secondColor(1)),

    decoration: InputDecoration(
    labelText: "Perfil Instagram",
    prefixIcon: Icon(Icons.camera),

    labelStyle:
    TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
    contentPadding: EdgeInsets.all(12),
    hintStyle: TextStyle(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.7)),
    border: OutlineInputBorder(
    borderSide: BorderSide(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.2))),
    focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.5))),
    enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
    color: Theme.of(context)
        .focusColor
        .withOpacity(0.2))),
    ),
    ),

      SizedBox(height: 20,),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 35),
        child: FadeAnimation(0.2, Container(
            height: 50,
            width: double.infinity,

            // margin: EdgeInsets.symmetric(horizontal: 50),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xFF464646)
            ),
            child: GestureDetector(
              onTap: () {
                if (_formKey.currentState!.validate()) {
                  this.updateProfile();
                }
              },
              child: Center(
                child: Text(AppLocalizations.of(context)!.save_changes, style: TextStyle(fontSize: 18,color: Color(0xFFcccccf), fontWeight: FontWeight.bold),),
              ),
            )
        )),
      ),

    ])),));
  }


  void updateProfile() async {
    try{
      Map<String,dynamic> dataUpdate = {"phone": txtPhone.text,"direction":txtdirection.text,
      "facebook": txtFacebook.text, "linkedin": txtLinkedIn.text, "instagram":txtInstagram.text};
      var responseRequest = await FetchServer.fetchPost("worker/updateProfile", dataUpdate);
      if(responseRequest.error || responseRequest.responseRequest == null){
        Util.showGenericError(context, ()=>{});
        return;
      }

      final Map<String, dynamic> responseData = jsonDecode(responseRequest.responseRequest.toString());
      var error = responseData['error'] ;
      String title = AppLocalizations.of(context)!.profile;
      if(error == null || error.toString().isEmpty || error.toString().toLowerCase() != "false"){
        Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.updateProfileError);
        return;
      }
      Util.showMessage(context, ()=>{}, title, AppLocalizations.of(context)!.updateProfileSuccess);

    }catch(e){
      print(e);
      Util.showGenericError(context, ()=>{});
    }
  }
  
}