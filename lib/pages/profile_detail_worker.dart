import 'dart:io';

import 'package:flutter/material.dart';
import 'package:management_location_mobile/config/app_config.dart' as config;
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:url_launcher/url_launcher_string.dart';

import '../model/customer.dart';

class ProfileDetailWorkerPage extends StatefulWidget{

  final Customer user;
  const ProfileDetailWorkerPage({Key? key,required this.user}) : super(key: key);

  @override
  StateProfileDetailWorkerPage createState() => StateProfileDetailWorkerPage();
}

class StateProfileDetailWorkerPage extends State<ProfileDetailWorkerPage>{

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).accentColor,
          elevation: 0,
          centerTitle: true,
          title: Text(
            AppLocalizations.of(context)!.profile,
            style: Theme.of(context)
                .textTheme.subtitle1!
                .merge(TextStyle(letterSpacing: 1.3,color:Colors.white)),
          ),
        ),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
        Container(
        child: Column(
        children: <Widget>[
        Container(
        margin: EdgeInsets.symmetric(
            horizontal: 0, vertical: 10),
        height: 200,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment:
          MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            SizedBox(
              width: 50,
              height: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  if(widget.user.phone == null || widget.user.phone!.isEmpty){
                    return;
                  }
                  launchUrlString(('tel://${widget.user.phone}'));
                },
                child: Icon(Icons.phone,
                    color:
                    Theme.of(context).primaryColor),
                color: Theme.of(context).accentColor,
                shape: StadiumBorder(),
              ),
            ),
            SizedBox(
              width: 200,
              height: 200,
              child: CircleAvatar(
                  backgroundImage:
                  NetworkImage(widget.user!.picture)),
            ),
            SizedBox(
              width: 50,
              height: 50,
              child: FlatButton(
                padding: EdgeInsets.all(0),
                onPressed: () {
                  openwhatsapp();
                },
                child: Icon(Icons.chat,
                    color:
                    Theme.of(context).primaryColor),
                color: Theme.of(context).accentColor,
                shape: StadiumBorder(),
              ),
            ),
          ],
        ),
      )])),
            SizedBox(height: 5,),
            Center(child:Text(
              AppLocalizations.of(context)!.contact_data,
              style: Theme.of(context)
                  .textTheme.subtitle1!
                  .merge(TextStyle(fontSize: 15.0,color:Colors.white)),
            )),
            Container(
                margin: const EdgeInsets.symmetric(
                    horizontal: 5, vertical: 10),
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(6),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context)
                            .hintColor
                            .withOpacity(0.15),
                        offset: Offset(0, 3),
                        blurRadius: 10)
                  ],
                ),child: getContactData()),
            SizedBox(height: 5,),
            Center(child:Text(
              AppLocalizations.of(context)!.categories_title,
              style: Theme.of(context)
                  .textTheme.subtitle1!
                  .merge(TextStyle(fontSize: 15.0,color:Colors.white)),
            )),
            Container(
              width: double.infinity,
                margin: const EdgeInsets.symmetric(
                    horizontal: 5, vertical: 10),
                decoration: BoxDecoration(
                  color: Theme.of(context).accentColor,
                  borderRadius: BorderRadius.circular(6),
                  boxShadow: [
                    BoxShadow(
                        color: Theme.of(context)
                            .hintColor
                            .withOpacity(0.15),
                        offset: Offset(0, 3),
                        blurRadius: 10)
                  ],
                ),child: Padding(padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),child:Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    mainAxisSize: MainAxisSize.max,
    children: getCategories()))),






    ],),),);
  }

  List<Widget> getCategories() {
    if(widget.user.categoryExperience == null) return [];
    List<Widget> widgetList = [];

    for(var item in widget.user.categoryExperience!){
      widgetList.add(Chip(
        avatar: CircleAvatar(
          backgroundColor: Colors.grey.shade800,
          child: Text(item.name.substring(0,1)),
        ),
        label: Text(item.name),
      ));
    }
    return widgetList;
  }
  Widget getContactData() {
    return Padding(padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[


          SizedBox(height: 20,),



          TextFormField(
            initialValue: widget.user.phone,
            enabled: false,
            style: TextStyle(color: config.Colors().secondColor(1)),
            keyboardType: TextInputType.text,
            maxLines: 1,
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.phone),
              labelText: "Telefono",
              labelStyle:
              TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
              contentPadding: EdgeInsets.all(12),
              hintStyle: TextStyle(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.7)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.5))),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
            ),
          ),
          SizedBox(height: 20,),

          TextFormField(
            initialValue: widget.user.direction,
            enabled: false,
            keyboardType: TextInputType.text,
            maxLines: 3,
            style: TextStyle(color: config.Colors().secondColor(1)),

            decoration: InputDecoration(
              labelText: "Dirección",
              prefixIcon: Icon(Icons.people),

              labelStyle:
              TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
              contentPadding: EdgeInsets.all(12),
              hintStyle: TextStyle(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.7)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.5))),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
            ),
          ),
          SizedBox(height: 20,),

          TextFormField(
            initialValue: widget.user.facebook,
            enabled: false,
            keyboardType: TextInputType.text,
            maxLines: 1,
            style: TextStyle(color: config.Colors().secondColor(1)),

            decoration: InputDecoration(
              labelText: "Perfil Facebook",
              prefixIcon: Icon(Icons.facebook),

              labelStyle:
              TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
              contentPadding: EdgeInsets.all(12),
              hintStyle: TextStyle(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.7)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.5))),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
            ),
          )
          ,
          SizedBox(height: 20,),

          TextFormField(
            initialValue: widget.user.linkedin,
            enabled: false,

            keyboardType: TextInputType.text,
            maxLines: 1,
            style: TextStyle(color: config.Colors().secondColor(1)),

            decoration: InputDecoration(
              labelText: "Perfil LinkedIn",
              prefixIcon: Icon(Icons.work),

              labelStyle:
              TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
              contentPadding: EdgeInsets.all(12),
              hintStyle: TextStyle(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.7)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.5))),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
            ),
          ),
          SizedBox(height: 20,),

          TextFormField(
            initialValue: widget.user.instagram,
            enabled: false,
            keyboardType: TextInputType.text,
            maxLines: 1,
            style: TextStyle(color: config.Colors().secondColor(1)),

            decoration: InputDecoration(
              labelText: "Perfil Instagram",
              prefixIcon: Icon(Icons.camera),

              labelStyle:
              TextStyle(color: Theme.of(context).focusColor.withOpacity(0.4)),
              contentPadding: EdgeInsets.all(12),
              hintStyle: TextStyle(
                  color: Theme.of(context)
                      .focusColor
                      .withOpacity(0.7)),
              border: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.5))),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Theme.of(context)
                          .focusColor
                          .withOpacity(0.2))),
            ),
          ),



        ]));


  }

  openwhatsapp() async{
    if(widget.user.phone == null || widget.user.phone!.isEmpty) return;
    var whatsappURl_android = "whatsapp://send?phone="+widget.user.phone!+"&text=Hola me gusta tu perfil quisiera que te pongas en contacto conmigo";
    var whatappURL_ios ="https://wa.me/$widget.user.phone!?text=${Uri.parse("Hola me gusta tu perfil quisiera que te pongas en contacto conmigo")}";
    if(Platform.isIOS){
      // for iOS phone only
      if( await canLaunchUrlString(whatappURL_ios)){
        await launchUrlString(whatappURL_ios);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no installed")));
      }
    }else{
      // android , web
      if( await canLaunchUrlString(whatsappURl_android)){
        await launchUrlString(whatsappURl_android);
      }else{
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: new Text("whatsapp no installed")));
      }
    }
  }

}